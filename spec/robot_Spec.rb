require "./lib/robot"

describe Robot do

	it "al tablero del robot dado como 0001001000100000 arriesgo a la cordenada d-1 obtengo Encontraste un bug!" do
		juego = Robot.new
		juego.matrixrobot = "0001001000100000"
		result = juego.findBug("d", "1")
		result.should == "Encontraste un bug!"
	end

	it "al tablero del robot dado como 0000001000100000 arriesgo a la cordenada d-1 obtengo Fallido" do
		juego = Robot.new
		juego.matrixrobot = "0000001000100000"
		result = juego.findBug("d", "1")
		result.should == "Fallido"
	end

end
