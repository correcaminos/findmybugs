require "./lib/game"

describe Game do

	it "al tablero dado como 00000000000000000 agrego la cordenada a-1 obtengo el tablero 1000000000000000" do
		juego = Game.new
		juego.addBug("a", "1")
		juego.matrix.should == "1000000000000000"
	end

end
