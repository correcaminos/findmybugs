require "./lib/robot"

describe Robot do

	it "al tablero dado como 00000000000000000 agrego el bug en la cordenada a-1, remuevo el bug en la cordenada a-1 y obtengo el tablero 0000000000000000" do
		juego = Robot.new
		juego.matrixrobot = "0000000000000000"
		juego.addBug("a", "1")
		juego.removeBug("a", "1")
		juego.matrixrobot.should == "0000000000000000"
	end

end
