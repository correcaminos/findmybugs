Given(/^entro a la aplicacion$/) do
  visit '/'
end

When(/^ingreso la coordenada  "(.*?)" "(.*?)"$/) do |fila, columna|
	select(fila, :from => 'fila')
	select(columna, :from => 'columna')
	click_button("Ingresar Bug")
end

Then(/^veo el mensaje "(.*?)"$/) do |text|
   last_response.body.should =~ /#{text}/m
end

Given(/^entro al tablero para jugar$/) do
	visit '/tablerojugar'
end

When(/^ingreso la coordenada "(.*?)" "(.*?)" dado el tablero "(.*?)"$/) do |fila, columna, tablero|
	select(fila, :from => 'fila')
	select(columna, :from => 'columna')
	@@matrixrobot = tablero
	click_button("jugar")
end

When(/^ingreso las coordenadas "(.*?)" "(.*?)"$/) do |f, c|
	select(f, :from => 'fila')
	select(c, :from => 'columna')
	click_button("Ingresar Bug")
end

When(/^pc arriesga coordenada "(.*?)" \- "(.*?)"$/) do |columna, fila|
 	@@columnapc = columna
  	@@filapc = fila
	visit '/jugarpc'
end

When(/^ingreso las coordenadas repetidas "(.*?)" "(.*?)" And "(.*?)" "(.*?)"$/) do |f1, c1, f2, c2|
  	select(f1, :from => 'fila')
	select(c1, :from => 'columna')
	click_button("Ingresar Bug")

	select(f2, :from => 'fila')
	select(c2, :from => 'columna')
	click_button("Ingresar Bug")
end



