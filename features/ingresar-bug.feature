Feature: Ingresar Bug

Scenario:Ingresar Coordenada
	Given entro a la aplicacion
	When ingreso la coordenada  "a" "2"	
	Then veo el mensaje "Coordenada ingresada!"

Scenario: Ingresar 4 Coordenadas
	Given entro a la aplicacion
	When ingreso las coordenadas "a" "1" 
	And ingreso las coordenadas "a" "2" 
	And ingreso las coordenadas "a" "3" 
	And ingreso las coordenadas "b" "1"
	Then veo el mensaje "Solo permite ingresar tres Bugs"

Scenario: Ingresar 3 Coordenadas para iniciar juego
	Given entro a la aplicacion
	When ingreso las coordenadas "a" "1" 
	And ingreso las coordenadas "a" "2" 
	And ingreso las coordenadas "a" "3"
	Then veo el mensaje "Iniciar Juego"

Scenario: Ingresar la misma coordenada dos veces
	Given entro a la aplicacion
	When ingreso las coordenadas repetidas "a" "1" And "a" "1"
	Then veo el mensaje "No puede ingresar la misma coordenada"

