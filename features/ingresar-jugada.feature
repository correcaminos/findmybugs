Feature: Ingresar Jugada

Scenario:Ingresar Coordenadas de Jugada
	Given entro a la aplicacion 
	And ingreso las coordenadas "a" "1"
	And ingreso las coordenadas "a" "2"
	And ingreso las coordenadas "a" "3"
	And entro al tablero para jugar
	When ingreso la coordenada "c" "1" dado el tablero "0010000010000000"
	Then veo el mensaje "Encontraste un bug!"

Scenario:Jugada Pc
	Given entro a la aplicacion 
	And ingreso las coordenadas "a" "1"
	And ingreso las coordenadas "a" "2"
	And ingreso las coordenadas "a" "3"
	And entro al tablero para jugar
	And ingreso la coordenada "c" "1" dado el tablero "0010000010000000" 
	When pc arriesga coordenada "a" - "1"
	Then veo el mensaje "El PC Jugo y Resolvio tu Bug."

Scenario:Jugada Pc
	Given entro a la aplicacion 
	And ingreso las coordenadas "a" "1"
	And ingreso las coordenadas "a" "2"
	And ingreso las coordenadas "a" "3"
	And entro al tablero para jugar
	And ingreso la coordenada "c" "1" dado el tablero "0010000010000000" 
	When pc arriesga coordenada "b" - "1"
	Then veo el mensaje "El PC Jugo y No logro entrar tu Bug."

Scenario: Ingresar 1 Coordenada en el tablero y ganar
	Given entro al tablero para jugar
	When ingreso la coordenada "a" "1" dado el tablero "1000000000000000" 
	Then veo el mensaje "Has Ganado"



