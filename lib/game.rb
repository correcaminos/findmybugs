class Game

	attr_accessor :matrix
	attr_accessor :alfabeto
	attr_accessor :bugs_activos
	def initialize
		@matrix = "0000000000000000"
		@alfabeto = "abcdefghi"
		@bugs_activos = 0
	end

	def addBug(fila, columna)
			a = (columna.to_i-1).to_i

			b = a * 4

			indexalfabeto = @alfabeto.to_s.index(fila)

			index = b  + indexalfabeto

			@matrix[index]="1"
			@bugs_activos = @bugs_activos+1
	end

	def allowAgregarBug()
		@bugs_activos < 3
	end

	def getValor(fila, columna)
		a = (columna.to_i-1).to_i
		b = a * 4
		indexalfabeto = @alfabeto.to_s.index(fila)
		index = b  + indexalfabeto
		@matrix[index]
	end	

	def isRepetido(fila, columna)
		getValor(fila,columna)=="1"
	end


	def isCleanTablero()
		@matrix.index("1")==nil
	end 

	def getPosicion(fila, columna)
		a = (columna.to_i-1).to_i
		b = a * 4
		indexalfabeto = @alfabeto.to_s.index(fila)
		index = b  + indexalfabeto
		index
	end

	def removeBug(fila, columna)
		@matrix[getPosicion(fila,columna)]="0"
	end	

	def findBug(fila, columna)
		if(getValor(fila,columna) == "1" )
			removeBug(fila,columna)
			"Encontraste un bug!"
		else
			"Fallido"
		end

	end


end
