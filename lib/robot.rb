class Robot
	attr_accessor :matrixrobot
	attr_accessor :alfabeto

	def initialize
		@matrixrobot = "0001001000100000"
		@alfabeto = "abcdefghi"
	end
	
	def getValor(fila, columna)
		@matrixrobot[getPosicion(fila,columna)]
	end	

	def getPosicion(fila, columna)
		a = (columna.to_i-1).to_i
		b = a * 4
		indexalfabeto = @alfabeto.to_s.index(fila)
		index = b  + indexalfabeto
		index
	end

	def removeBug(fila, columna)
		@matrixrobot[getPosicion(fila,columna)]="0"
	end	

	def addBug(fila, columna)
		@matrixrobot[getPosicion(fila,columna)]="1"
	end
	
	def findBug(fila, columna)
		if(getValor(fila,columna) == "1" )
			removeBug(fila,columna)
			"Encontraste un bug!"
		else
			"Fallido"
		end

	end

	def isCleanTablero()
		@matrixrobot.index("1")==nil
	end

	def getFila() 
		Random.rand(4) + 1 
	end

	def getColumna()
		@alfabeto[Random.rand(4)]
	end
	
end
