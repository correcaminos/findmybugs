require 'sinatra'
require './lib/game'
require './lib/robot'

configure do
  enable :sessions
end

get '/' do
	@tableroJugador1 = "00000000000000000"
	@@game = Game.new
	@tableroJugadas = "00000000000000000"
	erb :tablero
end

post '/ingresar_bug' do
	@fila = params[:fila]
	@columna = params[:columna]
	
	if(@@game.isRepetido(@fila,@columna))
		@mensaje = "No puede ingresar la misma coordenada!"

	else
		if(@@game.allowAgregarBug())
			@@game.addBug(@fila,@columna)
			@mensaje = "Coordenada ingresada!"
		else
			@mensaje = "Solo permite ingresar tres Bugs"
		end
	end
	@tableroJugador1 = @@game.matrix.split("")
	@verIniciar = 	!@@game.allowAgregarBug()
 	erb :tablero
end 

get '/iniciojuego' do
	
	erb :tablerojuego
end

get '/tablerojugar' do
	@@robot = Robot.new
	@@game2 = Game.new
	@@matrixrobot = @@robot.matrixrobot
	@tableroJugador1 = @@game.matrix
	@tableroJugadas = @@game2.matrix
	erb :tablerojuego
end

get '/tablerojuego' do
	erb :tablerojuego
end

post '/jugar' do
	@@robot.matrixrobot = @@matrixrobot

	
	@tableroJugador1 = @@game.matrix
	@fila = params[:fila]
	@columna = params[:columna]
	
	if(@@robot.getValor(@fila, @columna)=="1")
		@@game2.addBug(@fila, @columna);	
	end
	@tableroJugadas = @@game2.matrix
	@resultadoJugada = @@robot.findBug(@fila,@columna)

	@@filapc = @@robot.getFila() 
	@@columnapc = @@robot.getColumna()

	@resultPC  = @@game.findBug(@@columnapc,@@filapc)

	if(@resultPC == "Fallido")
 		@resultPC =  "No logro entrar tu Bug."
	else 
		@resultPC = "Resolvio tu Bug."
	end

	@jugadaPCColumna = @@robot.getColumna()
	@jugadaPCFila = @@robot.getFila()
	#+ "-"+ @@robot.getFila()
	#@@robot.getFila() + @@robot.getColumna()

	@@filapc = "1"
	@@columnapc = "a"
	if(@@robot.isCleanTablero())
		@msgTerminado = "Has Ganado"
		erb :fin
	else
		if(@@game.isCleanTablero())
			@msgTerminado = "Has Perdido"
			erb :fin
		else
			erb :tablerojuego
		end

	end
end

get '/jugarpc' do

	@tableroJugador1 = "00000000000000000"

	@tableroJugadas = "00000000000000000"
	@resultPC  = @@game.findBug(@@columnapc,@@filapc)

	if(@resultPC == "Fallido")
 		@resultPC =  "No logro entrar tu Bug."
	else 
		@resultPC = "Resolvio tu Bug."
	end

	erb :tablerojuego
end


